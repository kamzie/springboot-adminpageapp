<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"
    import="zielware.*"
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<!-- <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> -->
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" type="text/css" href="style/style.css" />
	<title>Logout Page</title>
</head>
<body>
	<form action="/logout" method="get">
		<div class="page_content">
			<h1>Logout Page</h1>
			<div class="background_content"> 
				<div class="main_content">
					<br>	
					Bye bye!
					<br>
					<input type="button" onclick="location.href='login.jsp';" 
						value="back to Login" />
				</div>
			</div>	
		</div>
	</form>	
</body>
</html>