<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" 
    import="zielware.*"
    import="zielware.domain.*"
    import="zielware.service.*"
	import="java.util.List"
	import="java.util.ArrayList"
%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- <script type="text/javascript" src="script/mouseHighlight.js"></script> -->
	<script type="text/javascript" src="script/selectItem.js"></script>
	<link rel="Stylesheet" type="text/css" href="style/style.css" />
	<title>Admin Page</title>
</head>
<body>
	<form action="admin" method="post">		
		<%
                            if(session.getAttribute("dbusers") == null){
                                 %> "Brak atrybutu sesji dbusers" <%
                                 return;
                            }
							List<UserEntity> users = new ArrayList<>();
							users = (List<UserEntity>) session.getAttribute("dbusers");

				%>	
		<div class="page_content"> 		
	 		<h1>Admin Page</h1>
			<div class="background_content">			
				<div class="main_content">
					<h3>Users:</h3>
					<center><table> 
						<tr>
							<th>Nr</th>
							<th>id</th>
							<th>login</th>
							<th>password</th>
							<th>username</th>
							<th>privileges</th>						
						</tr>
						<% 
						for(int i=0;i<users.size();i++){
						%>
							<tr class="odd" onclick="toggle(this, <%=users.get(i).getId() %> )">
								<td><%=i %></td>
								<td><%=users.get(i).getId() %></td>
								<td><%=users.get(i).getLogin() %></td>
								<td><%=users.get(i).getPassword() %></td>
								<td><%=users.get(i).getUsername() %></td>
								<td><%=users.get(i).isAdmin() %></td>
							</tr>	
						<% 
						}		
						%>
							<tr class="table_hover">
								<td><%=users.size()%></td>
								<td><% %></td>
								<td><input class="glowing-border" type="text" name="login_input" placeholder="login"/></td>
								<td><input class="glowing-border" type="text" name="password_input" placeholder="password"/></td>
								<td><input class="glowing-border" type="text" name="username_input" placeholder="username"/></td>
								<td><select name="privileges_select">
									<option>0</option>
									<option>1</option></select>
								</td>			
							</tr>							
					</table>
					<input type="hidden" name="user_to_delete" id="user_to_delete" value="">
					</center>
					<br>
					<% 
						String msg = (String) request.getAttribute("msg");
						if( msg == null){msg = "";}	
					%>
					Komunikat: <%=msg %>
					<br>				
					<input type="submit" formaction="/deleteUser" name="delete_user" value="Delete User" id="delete_user_button" style="width: 130px; " disabled/>
					<input type="submit" formaction="/createUser" name="create_user" value="Create User" style="width: 130px; "/>
					<br>
					<br>
				</div>	
				<br>
				    <center><input class="button" type="submit" formaction="/logout" name="logout" value="logout" style="width: 130px;"/></center>
				<br>	
				<center>@Kamil Zieliński</center>
			</div>
		</div>
	</form>
</body>
</html>