package zielware.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;

@Data
@AllArgsConstructor
public class UserDTO {

    private String login;
    private String password;
    private String username;
    private boolean isAdmin = false;

    public UserDTO(HttpServletRequest req) {
        this.login = req.getParameter("login_input");
        this.password = req.getParameter("password_input");
        this.username = req.getParameter("username_input");

        if (StringUtils.equals(req.getParameter("privileges_select"), "1") || login.equals("admin")) {
            isAdmin = true;
        }
    }
}
