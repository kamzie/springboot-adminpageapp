package zielware.service;

import zielware.domain.UserDTO;

public interface ValidationService {

    boolean isAnyBlank(UserDTO userModel);

    boolean isLoginTaken(String login);
}
