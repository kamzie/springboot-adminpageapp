package zielware.service;

import lombok.Getter;

public enum Communicates {

    FILL_FORM("Wypelnij caly formularz!"),
    WRONG_FORM("Bledne dane logowania!"),
    USER_EXISTS_ALREADY("Taki user juz istnieje!"),
    USER_DELETED("Usunieto usera!"),
    CREATE_USR_OK("Udalo sie stworzyc usera!");

    @Getter
    private String message;

    Communicates(String message) {
        this.message = message;
    }
}
