package zielware.service;

import zielware.domain.UserDTO;
import zielware.domain.UserEntity;

import java.util.List;

public interface UserService {

    List<UserEntity> getAllUsers();

    UserEntity getUser(long id);

    void addUser(UserDTO user);

    void deleteUserById(String id);
}
