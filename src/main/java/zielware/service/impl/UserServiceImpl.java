package zielware.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import zielware.domain.UserDTO;
import zielware.domain.UserEntity;
import zielware.repository.UserRepository;
import zielware.service.UserService;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    private static final String ADMIN = "admin";

    @PostConstruct
    public void initAdminUser() {
        addUser(new UserDTO(ADMIN, ADMIN, ADMIN, true));
    }

    public List<UserEntity> getAllUsers() {
        List<UserEntity> users = new ArrayList<>();
        userRepository.findAll().forEach(users::add);
        return users;
    }

    @Override
    public UserEntity getUser(long id) {
        int i = Math.toIntExact(id);
        return getAllUsers().get(i);
    }

    @Override
    public void addUser(UserDTO user) {
        UserEntity dbuser = new UserEntity();
        dbuser.setLogin(user.getLogin());
        dbuser.setPassword(user.getPassword());
        dbuser.setUsername(user.getUsername());
        dbuser.setAdmin(user.isAdmin());
        userRepository.save(dbuser);
    }

    @Override
    public void deleteUserById(String id) {
        userRepository.deleteById(Long.parseLong(id));
    }
}
