package zielware.service.impl;

import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import zielware.domain.UserDTO;
import zielware.service.ValidationService;

@Service
@RequiredArgsConstructor
public class ValidationServiceImpl implements ValidationService {

    private final UserServiceImpl userService;

    @Override
    public boolean isAnyBlank(UserDTO userModel) {
        return StringUtils.isAnyBlank(userModel.getLogin(), userModel.getPassword(), userModel.getUsername());
    }

    @Override
    public boolean isLoginTaken(String login) {
        return userService.getAllUsers().stream().anyMatch(u -> u.getLogin().equalsIgnoreCase(login));
    }
}