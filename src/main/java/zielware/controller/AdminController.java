package zielware.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import zielware.domain.UserDTO;
import zielware.service.ValidationService;
import zielware.service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static zielware.controller.ResponseForwarder.forwardResponseWithMsg;
import static zielware.service.Communicates.CREATE_USR_OK;
import static zielware.service.Communicates.FILL_FORM;
import static zielware.service.Communicates.USER_DELETED;
import static zielware.service.Communicates.USER_EXISTS_ALREADY;

@Controller
@RequiredArgsConstructor
public class AdminController {

    public final static String ADMIN_JSP = "admin.jsp";

    private final UserServiceImpl userDaoService;
    private final ValidationService validationService;

    private UserDTO userDto;

    @GetMapping(value = "/admin")
    public String doGet() {
        return "/admin";
    }

    @PostMapping("/createUser")
    public void createUser(HttpServletRequest req, HttpServletResponse resp) {
        userDto = new UserDTO(req);
        try {
            commitIfFormUncorrect(req, resp);
            commitIfFormCorrectAndCreateUser(req, resp);
        } catch (ServletException | IOException e) {
            e.printStackTrace();
        }
    }

    @PostMapping("/deleteUser")
    public void deleteUser(HttpServletRequest req, HttpServletResponse resp)
            throws IOException, ServletException {

        String idUserToDelete = req.getParameter("user_to_delete");
        userDaoService.deleteUserById(idUserToDelete);

        req.getSession().setAttribute("dbusers", userDaoService.getAllUsers());
        forwardResponseWithMsg(ADMIN_JSP, USER_DELETED.getMessage(), req, resp);
    }

    private void commitIfFormUncorrect(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        if (validationService.isAnyBlank(userDto)) {
            forwardResponseWithMsg(ADMIN_JSP, FILL_FORM.getMessage(), req, resp);
            return;
        }

        if (validationService.isLoginTaken(userDto.getLogin())) {
            forwardResponseWithMsg(ADMIN_JSP, USER_EXISTS_ALREADY.getMessage(), req, resp);
        }
    }

    private void commitIfFormCorrectAndCreateUser(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        if (resp.isCommitted()) return;

        userDaoService.addUser(userDto);
        req.getSession().setAttribute("dbusers", userDaoService.getAllUsers());

        forwardResponseWithMsg(ADMIN_JSP, CREATE_USR_OK.getMessage(), req, resp);
    }
}