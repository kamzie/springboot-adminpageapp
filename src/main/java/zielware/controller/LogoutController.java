package zielware.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class LogoutController {

	@PostMapping("/logout")
	public String doPost() {
		return "/logout";
	}

	@GetMapping("/logout")
	public String doGet() {
		return "/logout";
	}
}
