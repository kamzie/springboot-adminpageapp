package zielware.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import zielware.domain.UserDTO;
import zielware.service.UserService;
import zielware.service.ValidationService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static zielware.controller.LoginController.SUCCESS_JSP;
import static zielware.controller.ResponseForwarder.forwardResponse;
import static zielware.controller.ResponseForwarder.forwardResponseWithMsg;
import static zielware.service.Communicates.FILL_FORM;
import static zielware.service.Communicates.USER_EXISTS_ALREADY;

@Controller
@RequiredArgsConstructor
public class RegisterController {

    public static final String REGISTER_JSP = "register.jsp";

    private final UserService userService;
    private final ValidationService validationService;

    private UserDTO userDto;

    @GetMapping("/register")
    public String doGet() {
        return "/register";
    }

    @PostMapping("/register")
    public void registerPage(HttpServletRequest req, HttpServletResponse resp) {
        userDto = new UserDTO(req);
        try {
            commitIfFormUncorrect(req, resp);
            commitIfFormCorrectAndCreateUser(req, resp);
        } catch (ServletException | IOException e) {
            e.printStackTrace();
        }
    }

    private void commitIfFormUncorrect(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        if (validationService.isAnyBlank(userDto)) {
            forwardResponseWithMsg(REGISTER_JSP, FILL_FORM.getMessage(), req, resp);
            return;
        }

        if (validationService.isLoginTaken(userDto.getLogin())) {
            forwardResponseWithMsg(REGISTER_JSP, USER_EXISTS_ALREADY.getMessage(), req, resp);
        }
    }

    private void commitIfFormCorrectAndCreateUser(HttpServletRequest req, HttpServletResponse resp)
            throws IOException, ServletException {
        if (resp.isCommitted()) return;

        userService.addUser(userDto);
        req.getSession().setAttribute("username", userDto.getUsername());
        forwardResponse(SUCCESS_JSP, req, resp);
    }
}