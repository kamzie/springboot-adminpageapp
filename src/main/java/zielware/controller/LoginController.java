package zielware.controller;

import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import zielware.domain.UserEntity;
import zielware.service.ValidationService;
import zielware.service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

import static zielware.controller.AdminController.ADMIN_JSP;
import static zielware.controller.ResponseForwarder.forwardResponse;
import static zielware.controller.ResponseForwarder.forwardResponseWithMsg;
import static zielware.service.Communicates.FILL_FORM;
import static zielware.service.Communicates.WRONG_FORM;

@Controller
@RequiredArgsConstructor
public class LoginController {

    public static final String SUCCESS_JSP = "success.jsp";
    public static final String LOGIN_JSP = "login.jsp";

    private final UserServiceImpl userSevice;
    private final ValidationService validationService;

    private String login;
    private String password;

    @GetMapping(value = "/login")
    public String doGet() {
        return "/login";
    }

    @PostMapping("/login")
    public void loginPage(HttpServletRequest req, HttpServletResponse resp) {

        login = req.getParameter("login_input");
        password = req.getParameter("password_input");

        try {
            commitIfFormUncorrect(req, resp);
            commitIfFormCorrect(req, resp);
        } catch (ServletException | IOException e) {
            e.printStackTrace();
        }
    }

    private void commitIfFormUncorrect(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {

        if (StringUtils.isAnyBlank(login, password)) {
            forwardResponseWithMsg(LOGIN_JSP, FILL_FORM.getMessage(), req, resp);
        }
    }

    private void commitIfFormCorrect(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        if (resp.isCommitted()) return;

        final List<UserEntity> users = userSevice.getAllUsers();

        Optional<UserEntity> userEntity = users.stream().filter(
                user -> StringUtils.equals(user.getLogin(), login)
                        && StringUtils.equals(user.getPassword(), password))
                .findAny();

        if (userEntity.isPresent()) {
            if (userEntity.get().isAdmin()) {
                req.getSession().setAttribute("dbusers", users);
                forwardResponse(ADMIN_JSP, req, resp);
            } else {
                req.getSession().setAttribute("username", userEntity.get().getUsername());
                forwardResponse(SUCCESS_JSP, req, resp);
            }
        } else {
            forwardResponseWithMsg(LOGIN_JSP, WRONG_FORM.getMessage(), req, resp);
        }
    }
}