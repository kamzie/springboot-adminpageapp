package zielware.controller;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ResponseForwarder {

    public static void forwardResponse(String whichJsp, HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        forwardResponseWithMsg(whichJsp, "", req, resp);
    }

    public static void forwardResponseWithMsg(String whichJsp, String msg, HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        req.setAttribute("msg", msg);
        RequestDispatcher dispatcher = req.getRequestDispatcher(whichJsp);
        dispatcher.forward(req, resp);
    }
}