package zielware.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;
import zielware.domain.UserEntity;

public interface UserRepository extends CrudRepository<UserEntity, String> {

    @Transactional
    @Modifying
    int deleteById(long id);
}
