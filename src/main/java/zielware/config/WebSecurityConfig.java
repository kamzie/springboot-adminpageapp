package zielware.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    public static final String USER = "USER";
    public static final String ADMIN = "ADMIN";
    public static final String LOGIN_JSP = "/login.jsp";
    public static final String LOGOUT_JSP = "/logout.jsp";
    public static final String SUCCESS_JSP = "/success.jsp";

    static final String[] PUBLIC_URLS = new String[]{"/login*", "/register*", "/style/**", "/script/**"};
    static final String[] USER_URL = new String[]{"/success*"};
    static final String[] ADMIN_URL = new String[]{"/admin*"};


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .authorizeRequests()
                .antMatchers(USER_URL).hasRole(USER)
                .antMatchers(PUBLIC_URLS).permitAll() // allow all at home page
                .antMatchers(ADMIN_URL).hasRole(ADMIN)
                .and()
                .formLogin()
                .loginPage(LOGIN_JSP).permitAll()
                .defaultSuccessUrl(SUCCESS_JSP);

        http
                .logout()
                .logoutSuccessUrl(LOGOUT_JSP);
    }
}